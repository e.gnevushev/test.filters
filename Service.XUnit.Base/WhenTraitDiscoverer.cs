﻿using Xunit.Sdk;
using Xunit.Abstractions;
using System.Collections.Generic;

namespace Service.XUnit.Base
{
    public class WhenTraitDiscoverer : ITraitDiscoverer
    {
        /// <summary>
        /// The namespace of this class
        /// </summary>
        internal const string AssemblyName = nameof(Service) + "." + nameof(Service.XUnit) + "." + nameof(Service.XUnit.Base);

        /// <summary>
        /// The fully qualified name of this class
        /// </summary>
        internal const string TypeName = AssemblyName + "." + nameof(WhenTraitDiscoverer);

        public IEnumerable<KeyValuePair<string, string>> GetTraits(IAttributeInfo traitAttribute)
        {
            var value = traitAttribute.GetNamedArgument<string>("When");

            if (!string.IsNullOrWhiteSpace(value))
            {
                yield return new KeyValuePair<string, string>("When", value);
            }
        }
    }
}
