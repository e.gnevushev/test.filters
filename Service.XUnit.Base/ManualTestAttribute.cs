﻿using System;

namespace Service.XUnit.Base
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ManualTestAttribute : WhenTraitAttribute
    {
        public ManualTestAttribute() : base("Manual")
        {
        }
    }
}
