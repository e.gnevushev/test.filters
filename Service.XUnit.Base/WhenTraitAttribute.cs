﻿using System;
using Xunit.Sdk;

namespace Service.XUnit.Base
{
    [TraitDiscoverer(WhenTraitDiscoverer.TypeName, WhenTraitDiscoverer.AssemblyName)]
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    public abstract class WhenTraitAttribute : Attribute, ITraitAttribute
    {
        protected WhenTraitAttribute(string when)
        {
            When = when;
        }

        public string When { get; }
    }
}
