#### Демонстрация запуска выборочных тестов xunit

#####  Структура проекта:
  - `Service.XUnit.Base`: содержит [ManualTestAttribute](Service.XUnit.Base/ManualTestAttribute.cs)  
  - `Service.Tests`: unit тесты  
  - `Service.IntegrationTests`: Integration тесты (включая Manual)
  - `Solution Items`:
    - **RunTests.All.bat**: запускает все тесты в Solution [10 штук]
    - **RunTests.IntegrationWithoutManual.bat**: запускает все Integration-тесты, кроме Manual [3 штуки] 
    - **RunTests.UnitOnly.bat**: запускает все Unit-тесты [3 штуки]  

#####  Подробнее:
- [Выполнение выборочных тестов](https://docs.microsoft.com/ru-ru/dotnet/core/testing/selective-unit-tests?pivots=xunit)  
- [Синтаксис --filter](https://docs.microsoft.com/ru-ru/dotnet/core/tools/dotnet-test#filter-option-details)
- [Custom Trait attribute example](https://github.com/xunit/samples.xunit/tree/main/TraitExtensibility)
