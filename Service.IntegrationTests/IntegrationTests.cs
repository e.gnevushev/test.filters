using Service.XUnit.Base;
using Xunit;

namespace Service.IntegrationTests
{
    public class IntegrationTests
    {
        [Fact]
        public void Test_Integration_One() { }

        [Fact]
        public void Test_Integration_Two() { }

        [Fact]
        public void Test_Integration_Three() { }

        [Fact, ManualTest]
        public void Test_Manual_From_Integration() { }
    }
}
