﻿using Xunit;
using Service.XUnit.Base;

namespace Service.IntegrationTests
{
    [ManualTest]
    public class ManualTests
    {
        [Fact]
        public void Test_Manual_One() { }

        [Fact]
        public void Test_Manual_Two() { }

        [Fact]
        public void Test_Manual_Three() { }
    }
}
